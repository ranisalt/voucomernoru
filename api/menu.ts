import { NowRequest, NowResponse } from "@vercel/node";

import { initializeApp } from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAjcweGesi0m3scDQOsCrQG-hUQuzkn29g",
  authDomain: "voucomernoru.firebaseapp.com",
  databaseURL: "https://voucomernoru.firebaseio.com",
  projectId: "voucomernoru",
  storageBucket: "voucomernoru.appspot.com",
  messagingSenderId: "836982048802",
  appId: "1:836982048802:web:db640347708b845623e27b",
  measurementId: "G-WVXNGQXYBS",
};

const firebase = initializeApp(firebaseConfig);

export default (req: NowRequest, res: NowResponse) => {
  firebase
    .firestore()
    .collection("menu")
    .orderBy("date", "desc")
    .limit(1)
    .get()
    .then(({ docs: [doc] }) => res.status(200).send(doc.data()))
    .catch(() => res.status(500));
};
