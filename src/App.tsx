import React from "preact/compat";
import { IonApp } from "@ionic/react";
import { Footer, Header, Menu, MenuProvider } from "./components";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Theme variables */
import "./theme/variables.css";

export const App: React.FC = () => (
  <IonApp>
    <Header />
    <MenuProvider>
      <Menu />
      <Footer />
    </MenuProvider>
  </IonApp>
);
