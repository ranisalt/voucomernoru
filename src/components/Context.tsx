import React, { createContext, useContext } from "preact/compat";
import { useAsync } from "react-use";
import { Spinner } from ".";

export interface MenuEntry {
  date: { nanoseconds: number; seconds: number };
  dessert: string;
  meat: string;
  salad: string;
  sauce: string;
  side: string;
}

const Context = createContext({} as MenuEntry);
const { Provider } = Context;

export const MenuProvider: React.FC = ({ children }) => {
  const { loading, value, error } = useAsync(() =>
    fetch("/api/menu").then((r) => r.json())
  );

  if (loading || error) return <Spinner />;
  return <Provider value={value as MenuEntry}>{children}</Provider>;
};

export const useMenu = () => useContext(Context);
