import React from "preact/compat";
import { IonFooter, IonTitle, IonToolbar } from "@ionic/react";
import { useMenu } from ".";

const formatter = new Intl.DateTimeFormat("pt-BR", {
  dateStyle: "full",
} as any);
export const Footer: React.FC = () => {
  const {
    date: { seconds },
  } = useMenu();
  return (
    <IonFooter>
      <IonToolbar>
        <IonTitle>{formatter.format(new Date(seconds * 1000))}</IonTitle>
      </IonToolbar>
    </IonFooter>
  );
};
