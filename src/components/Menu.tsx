import React from "preact/compat";
import { IonContent, IonItem, IonList } from "@ionic/react";
import { useMenu } from ".";

export const Menu: React.FC = () => {
  const { meat, side, salad, sauce, dessert } = useMenu();
  return (
    <IonContent>
      <IonList lines="none">
        {[meat, side, salad, sauce, dessert].filter(Boolean).map((item) => (
          <IonItem key={item}>{item}</IonItem>
        ))}
      </IonList>
    </IonContent>
  );
};
