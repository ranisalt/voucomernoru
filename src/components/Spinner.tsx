import React from "preact/compat";
import { IonSpinner } from "@ionic/react";

export const Spinner: React.FC = () => (
  <div
    style={{
      alignItems: "center",
      display: "flex",
      flex: 1,
      justifyContent: "center",
    }}
  >
    <IonSpinner />
  </div>
);
